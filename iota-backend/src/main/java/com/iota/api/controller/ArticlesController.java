package com.iota.api.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iota.api.domain.Article;
import com.iota.api.repository.ArticleRepository;

@RestController
@RequestMapping("/articles")
public class ArticlesController {

    private final ArticleRepository articleRepository;

    public ArticlesController(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @GetMapping
    public List<Article> getArticles() {
        return articleRepository.findAll();
    }

    @GetMapping("/{id}")
    public Article getArticle(@PathVariable Long id) {
        return articleRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    @PostMapping
    public ResponseEntity<Article> createArticle(@RequestBody Article Article) throws URISyntaxException {
        Article savedArticle = articleRepository.save(Article);
        return ResponseEntity.created(new URI("/articles/" + savedArticle.getId())).body(savedArticle);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Article> updateArticle(@PathVariable Long id, @RequestBody Article article) {
        Article currentArticle = articleRepository.findById(id).orElseThrow(RuntimeException::new);
        currentArticle.setName(article.getName());
        currentArticle.setPrice(article.getPrice());
        currentArticle = articleRepository.save(article);

        return ResponseEntity.ok(currentArticle);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteArticle(@PathVariable Long id) {
        articleRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

}
