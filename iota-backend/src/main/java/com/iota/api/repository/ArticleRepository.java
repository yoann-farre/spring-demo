package com.iota.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iota.api.domain.Article;

public interface ArticleRepository extends JpaRepository<Article, Long> {
}
