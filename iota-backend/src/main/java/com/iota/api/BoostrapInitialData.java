package com.iota.api;

import java.util.Locale;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;
import com.iota.api.domain.Article;
import com.iota.api.domain.Client;
import com.iota.api.repository.ArticleRepository;
import com.iota.api.repository.ClientRepository;

@Component
public class BoostrapInitialData implements CommandLineRunner {

    private final ClientRepository clientRepository;
    private final ArticleRepository articleRepository;
    private final Faker faker = new Faker(Locale.getDefault());

    public BoostrapInitialData(ClientRepository clientRepository, ArticleRepository articleRepository) {
        this.clientRepository = clientRepository;
        this.articleRepository = articleRepository;
    }

    @Override
    public void run(String... args) {
        for (int i = 0; i < 10; i++) {
            clientRepository.save(new Client(faker.name().fullName(), faker.internet().emailAddress()));
            articleRepository.save(new Article(faker.book().title(), Double.valueOf(faker.commerce().price().replaceAll(",", "."))));
        }
    }
}
